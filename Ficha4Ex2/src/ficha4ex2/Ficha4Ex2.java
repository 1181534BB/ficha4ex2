/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha4ex2;
import java.util.Scanner;

/**
 *
 * @author Bruno
 */
public class Ficha4Ex2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner scanner = new Scanner(System.in);
        int n =0;
        
        while(n == 0){
            System.out.println("1 - Converter de Decimal para Binário" + "\n"
                    + "2 - Converter de Binário para Decimal" + "\n"
                    + "3 - Converter de Octal para Binário" + "\n"
                    + "4 - Converter de Binário para Octal" + "\n"
                    );
            n = scanner.nextInt();
            switch(n){
                case 1: conversaoDecimalBinario();
                n = 0;
                break;
                case 2: conversaoBinarioDecimal();
                n = 0;
                break;
                case 3: conversaoOctalBinario();
                n = 0;
                break;
                case 4: conversaoBinarioOctal();
                n = 0;
                break;
                
                default:
                    n = 1;
                break;
    }}
 
    }
    
public static void conversaoDecimalBinario(){
    Scanner scanner = new Scanner(System.in);
    int numDecimal=0;
    
    System.out.println("Digite o nº decimal a converter para binario: ");
    numDecimal = scanner.nextInt();
    System.out.println(Integer.toBinaryString(numDecimal));
}
public static void conversaoBinarioDecimal() {
int decimal;
String binario;

Scanner scanner=new Scanner(System.in);

System.out.println("Introduza o número binário a converter para decimal");
binario=scanner.nextLine();
System.out.println("O número binario " +binario+ " é " +Integer.parseInt(binario,2)+ " em decimal ");

    


}   
 
    public static void conversaoOctalBinario() {
    int octa= 0;
    
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Digite o número octal que pretende converter para binário.");
    octa=scan.nextInt();
        System.out.println(Integer.toBinaryString(octa));
   }
public static void conversaoBinarioOctal(){
    String numBinario;
    Scanner scanner = new Scanner(System.in);
    
    System.out.println("Digite o nº binario a converter para octal: ");
    numBinario = scanner.nextLine();
    //System.out.println(Integer.toOctalString(numBinario));
    System.out.println(Integer.toOctalString(Integer.parseInt(numBinario, 2)));
}
}
